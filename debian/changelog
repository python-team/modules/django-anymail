django-anymail (12.0-1) unstable; urgency=medium

  * New upstream version 12.0
  * d/control:
    - Set python3-django dependency minimum version to 4.0
  * d/p/0002-Remove-Django-5.1-Classifier-from-pyproject-toml.patch:
    - Add a new patch to fix unknown classifier build error

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Sun, 15 Sep 2024 06:17:34 +0000

django-anymail (11.1-1) unstable; urgency=medium

  * New upstream version 11.1

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Mon, 12 Aug 2024 14:19:58 +0000

django-anymail (11.0.1-1) unstable; urgency=medium

  * New upstream version 11.0.1

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Thu, 18 Jul 2024 14:44:20 +0000

django-anymail (11.0-1) unstable; urgency=medium

  * New upstream version 11.0

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Tue, 02 Jul 2024 13:25:53 +0000

django-anymail (10.3-2) unstable; urgency=medium

  * d/control: Remove python3-sparkpost from suggested packages (not needed)

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Thu, 23 May 2024 10:06:11 +0000

django-anymail (10.3-1) unstable; urgency=medium

  * New upstream version 10.3
  * New Maintainer (Closes: #1063317)
  * d/control:
    - Set Rules-Requires-Root to 'no'
    - Bump Standards-Version to 4.7.0
    - Change Vcs-Browser to correct URL
    - Change Maintainer name to new maintainer
    - Add Build-Depends on python3-django, python3-responses, and
      python3-boto3; needed by tests
  * d/copyright: Add New maintainer's name in debian/* copyright stanza
  * d/p/0001-No-hatchling-README-install.patch: modify according to upstream
  * d/watch: Update watch file to watch github instead of pypi
  * d/rules:
    - Change PYBUILD_NAME to 'anymail'
    - Override dh_auto_test to run tests
    - Override dh_installchangelogs to include CHANGELOG.rst
  * d/tests/control: setup autopkgtest test command

 -- Akash Doppalapudi <akashdoppalapudi2001@gmail.com>  Sun, 19 May 2024 06:34:45 +0000

django-anymail (10.2-1) unstable; urgency=medium

  * Orphan the package, see #1063317
    - Remove myself from uploaders and update maintainer to Debian QA Group
    - Update Vcs-* to Debian group
  * Add d/source/options extend-diff-ignore to fix dpkg-source failure due to
    local changes (python package metadata regeneration) (Closes: #1044967)
  * Update d/watch to find django_anymail so versions after 9.2 are detected
  * New upstream release
  * Replace Build-Depends on python3-setuptools with pybuild-plugin-pyproject
    and python3-hatchling
  * Add d/p/0001-No-hatchling-README-install.patch to avoid stray README
    install

 -- Scott Kitterman <scott@kitterman.com>  Tue, 06 Feb 2024 00:41:53 -0500

django-anymail (9.2-1) unstable; urgency=medium

  * New upstream release
  * Update package long description to mention support for MailerSend
  * Add python3-cryptography to Suggests and menition it is required for
    Postal support in long description

 -- Scott Kitterman <scott@kitterman.com>  Thu, 15 Jun 2023 18:35:19 -0400

django-anymail (9.0-1) unstable; urgency=medium

  * Delete obsolete debian/NEWS entry
  * New upstream release
  * Bump standards-version to 4.6.2 with no further changes

 -- Scott Kitterman <scott@kitterman.com>  Wed, 21 Dec 2022 00:30:53 -0500

django-anymail (8.6-1) unstable; urgency=medium

  * New upstream release
  * Update long description based on upstream README updates
  * Bump standards-version to 4.6.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Tue, 25 Oct 2022 23:35:43 -0400

django-anymail (8.5-1) unstable; urgency=medium

  * New upstream release
  * Update debian/watch to version 4
  * Add repository fields to d/u/metadata

 -- Scott Kitterman <scott@kitterman.com>  Fri, 21 Jan 2022 16:19:26 -0500

django-anymail (8.4-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Scott Kitterman ]
  * New upstream release
  * Update debhelper-compat to 13
  * Bump standards-verstion to 4.6.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Fri, 10 Dec 2021 22:52:06 -0500

django-anymail (7.1.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Fix wrong Vcs-*

  [ Scott Kitterman ]
  * New upstream release
  * Bump standards-version to 4.5.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Tue, 14 Apr 2020 22:53:44 -0400

django-anymail (7.0.0-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Thu, 19 Sep 2019 19:15:28 -0400

django-anymail (6.1.0-1) unstable; urgency=medium

  * Bump standards-version to 4.4.0 without further change
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 03 Aug 2019 18:22:24 -0400

django-anymail (5.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Team upload.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Mon, 22 Jul 2019 03:42:18 +0200

django-anymail (5.0-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.2.1 without further change
  * Move from contrib to main - free software that interacts with
    proprietary services is allowed in main

 -- Scott Kitterman <scott@kitterman.com>  Wed, 05 Dec 2018 15:30:07 -0500

django-anymail (3.0-1) unstable; urgency=medium

  * New upstream release
  * Add python/python3-boto3 to Suggests for Amazon SES support

 -- Scott Kitterman <scott@kitterman.com>  Mon, 04 Jun 2018 01:07:47 -0400

django-anymail (2.2-1) unstable; urgency=medium

  * New upstream release
    - Add Amazon SES to supported mailers in long descriptions
  * Bump standards-version to 4.1.4 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 30 Apr 2018 17:12:23 -0400

django-anymail (2.0-1) unstable; urgency=medium

  * New upstream release
    - Updated package descriptions in debian/control to mention addition of
      SendinBlue support
    - Add debian/NEWS to report breaking change, WEBHOOK_AUTHORIZATION renamed
      to WEBHOOK_SECRET

 -- Scott Kitterman <scott@kitterman.com>  Sun, 11 Mar 2018 03:42:20 -0400

django-anymail (1.4-1) unstable; urgency=high

  * New upstream release (Closes: #890097)
    - Fixes new WEBHOOK_AUTHORIZATION secret security issue (CVE pending)
  * Update Vcs-* for move to salsa.d.o
  * Bump standards-version to 4.1.3 without further change
  * Added missing disclaimer on why django-anymail is in contrib

 -- Scott Kitterman <scott@kitterman.com>  Sun, 11 Feb 2018 01:21:39 -0500

django-anymail (1.3-1) unstable; urgency=medium

  * New upstream release (Closes: #889450)
    - Includes security fix for timing attack on WEBHOOK_AUTHORIZATION secret
      (CVE-2018-6596) as described in
      https://github.com/anymail/django-anymail/releases/tag/v1.2.1
  * Update debian/watch and debian/copyright to use secure URIs

 -- Scott Kitterman <scott@kitterman.com>  Sat, 03 Feb 2018 11:23:43 -0500

django-anymail (1.2-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sun, 05 Nov 2017 18:29:24 -0500

django-anymail (1.0-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.1.1.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 07 Oct 2017 18:49:50 -0400

django-anymail (0.11.1-1) unstable; urgency=medium

  * New upstream release
  * Bump standards version to 4.0.1 without further change
  * Fix debian/changelog typo
  * Update package descriptions to mention support for Mailjet

 -- Scott Kitterman <scott@kitterman.com>  Sun, 20 Aug 2017 15:13:37 -0400

django-anymail (0.10-1) unstable; urgency=medium

  * New upstream release
    - Drop obsolete patches and debian/patches directory

 -- Scott Kitterman <scott@kitterman.com>  Mon, 19 Jun 2017 20:58:07 -0400

django-anymail (0.8-2) unstable; urgency=medium

  * Upload to unstable (See #859306)
  * Cherry pick debian/patches/fix-attachment-crash from upstream to prevent
    tracebacks when sending rfc822 messages as attachments (Upstream
    issue https://github.com/anymail/django-anymail/pull/59)

 -- Scott Kitterman <scott@kitterman.com>  Sat, 22 Apr 2017 15:41:48 -0400

django-anymail (0.8-1) experimental; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 01 Apr 2017 16:37:22 -0400

django-anymail (0.7-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Mon, 02 Jan 2017 08:15:39 -0500

django-anymail (0.6.1-1) unstable; urgency=low

  * Initial release. (Closes: #844675)

 -- Scott Kitterman <scott@kitterman.com>  Thu, 17 Nov 2016 18:15:32 -0500
